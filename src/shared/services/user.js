import { apiUrls, authenticationToken } from "../../config";
import { hashPassword } from "./password";

export function createUser() {
  const url = window.location.href

  let token = url.substring(url.indexOf("=") + 1, url.length);

  return fetch(`${apiUrls.createUser}/${token}`);
}

export function updateUserProfile(profileData, passwordData) {
  let body = JSON.stringify({
    username: profileData.username,
    email: profileData.email,
    password: hashPassword(passwordData.newPassword),
    oldPassword: hashPassword(passwordData.oldPassword),
    gender: profileData.gender,
    pictureUrl: '../../shared/assets/poop1.png',
  });

  return fetch(apiUrls.update, {
    method: "PUT",
    headers: {
      "Content-Type" : "application/json",
      "Authorization" : `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
    },
    body: body
  });
}

export function getCurrentUser() {
  return fetch(apiUrls.currentUser, {
    method: "GET",
    headers: {
      "Authorization" : `Bearer ${window.sessionStorage.getItem(authenticationToken)}`,
      "Content-Type" : "application/json"
    }
  })
}

export function deleteUserProfile() {

  let token = window.sessionStorage.getItem(authenticationToken);

  window.sessionStorage.removeItem(authenticationToken);

  return fetch(apiUrls.deleteOwnProfile, {
    method: "DELETE",
    headers: {
      "Authorization" : `Bearer ${token}`,
      "Content-Type" : "application/json"
    }
  });
}

export function checkStatus(user) {
  if (user.confirmed) {
    if (user.enabled) {
      return "Active";
    } else {
      return "Blocked";
    }
  } else {
    return "Not Confirmed";
  }
}