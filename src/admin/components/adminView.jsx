import React, { Component } from 'react';
import Filter from './filter';

import { routes } from '../../config';
import UserProfileAdmin from './userProfileAdmin';
import { getAllUsers, searchUser, adminBlockUser, adminUnblockUser } from '../../shared/services/admin';
import LoadingScreen from '../../shared/components/loadingScreen';
import { checkStatus } from '../../shared/services/user';

class AdminView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            usernameToSearch: ''
        }

        this.fillUsers();
    }

    render() {
        return (
            this.state.users.length > 0 ?
            <div className="container">
                <div className="row">
                    <div className="col s2">
                        <a className="waves-effect waves-light btn logout" href={routes.login} id="logout">LOGOUT</a>
                    </div>
                    <div className="col s4">
                        <a className="waves-effect waves-light btn" href={routes.startScreen} id="home">HOME</a>
                    </div>
                </div>
                <div className="row filter">
                    <Filter 
                        updateUsernameToSearch={(e) => this.updateUsernameToSearch(e.target.value)}
                        filter={() => this.filter()}
                        removeFilters={() => this.fillUsers()}
                    />
                </div>
                {this.state.users.slice(0, 5).map((user) => (
                    <UserProfileAdmin 
                        key={user.email}
                        username={user.username}
                        highscore={user.highscore} 
                        status={checkStatus(user)}
                        blockUser={() => this.blockUser(user)}
                        unblockUser={() => this.unblockUser(user)}
                   />
                ))}
            </div>
            : <LoadingScreen />
        );
    }


    updateUsernameToSearch(username) {
        this.setState({usernameToSearch: username});
    }

    filter() {
        searchUser(this.state.usernameToSearch).then((result) => {
            result.text().then((users) => {
                this.setState({users: JSON.parse(users)});
            })
        });
    }

    fillUsers() {
        getAllUsers().then((result) => {
            result.text().then((users) => {
                this.setState({users: JSON.parse(users)});
            })
        });
    }

    blockUser(user) {
        adminBlockUser(user).then((result) => {
            if (result.status === 200) {
                this.fillUsers();
            } else {
                alert('Something went wrong, try again later');
            }
        })
    }

    unblockUser(user) {
        adminUnblockUser(user).then((result) => {
            if (result.status === 200) {
                this.fillUsers();
            } else {
                alert('Something went wrong, try again later');
            }
        })
    }
}

export default AdminView;