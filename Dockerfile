FROM node:10.14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm i filepond-plugin-image-exif-orientation
RUN npm i filepond-plugin-image-preview
RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

ENV BACKEND_PORT 8444
ENV PROTOCOL https

#CMD sed -i -e 's/{backEndIp}:8444/{backEndIp}:'"${BACKEND_PORT}"'/g' src/config.js && sed -i -e 's/https/'"${PROTOCOL}"'/g' src/config.js && npm start
ENTRYPOINT ["npm", "start"]
